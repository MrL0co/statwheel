<?php
$meta['table']            = array('onoff');
$meta['radius']           = array('numeric', '_min' => 0);
//$meta['legend']           = array('onoff');
$meta['stepsize']         = array('numeric', '_min' => 0);
$meta['valuestrokealpha'] = array('numeric', '_min' => 0, 'max' => 1);
$meta['valuestrokecolor'] = array('string');
$meta['valuestrokesize']  = array('numeric', '_min' => 0);
$meta['gradient_inner']   = array('string');
$meta['gradient_outer']   = array('string');
$meta['gridcolor']        = array('string');
$meta['gridsize']         = array('numeric', '_min' => 0);
$meta['labels']           = array('onoff');
$meta['labelcolor']       = array('string');
$meta['labelfont']        = array('string');