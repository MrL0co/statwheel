<?php
$conf['table']            = true;
$conf['radius']           = 155;
//$conf['legend']           = false;
$conf['stepsize']         = 26;
$conf['valuestrokealpha'] = .5;
$conf['valuestrokecolor'] = '#045';
$conf['valuestrokesize']  = 2;
$conf['gradient_inner']   = "#ffd505";
$conf['gradient_outer']   = "#f12d05";
$conf['gridcolor']        = '#000';
$conf['gridsize']         = 2;
$conf['labels']           = true;
$conf['labelcolor']       = '#000';
$conf['labelfont']        = '';//'Handel Gothic.ttf';