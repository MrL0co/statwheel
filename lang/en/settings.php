<?php
$lang['table']            = 'Add table';
$lang['radius']           = 'Radius of the wheel';
//$lang['legend']           = 'show legend';
$lang['stepsize']         = 'Spacing between grid lines';
$lang['valuestrokealpha'] = 'Transparency level for the value overlay';
$lang['valuestrokecolor'] = 'Color for the value overlay';
$lang['valuestrokesize']  = 'Size of the border for the value overlay';
$lang['gradient_inner']   = 'Inner gradient color';
$lang['gradient_outer']   = 'Outer gradient color';
$lang['gridcolor']        = 'Color of the grid';
$lang['gridsize']         = 'Grid line size';
$lang['labels']           = 'Show names';
$lang['labelcolor']       = 'Name color';
$lang['labelfont']        = 'Name font';