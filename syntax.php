<?php
/**
 * Stat Wheel Plugin: Generates a Stat Wheel graph
 *
 * @license    TBD
 * @author     Richard Jacobse <richard@mrl0co.com>
 */
// must be run within Dokuwiki
if(!defined('DOKU_INC')) die();

include dirname(__FILE__).'/image.php';
/**
 * All DokuWiki plugins to extend the parser/rendering mechanism
 * need to inherit from this class
 */
class syntax_plugin_statwheel extends DokuWiki_Syntax_Plugin {

  /**
   * What kind of syntax are we?
   */
  function getType(){
    return 'substition';
  }

  function getAllowedTypes(){
    return array();
  }

  /**
   * What about paragraphs?
   */
  function getPType(){
    return 'block';
  }

  /**
   * Connect pattern to lexer
   */
  function connectTo($mode) {
    $this->Lexer->addEntryPattern('<statwheel(?:[^>]*?)>', $mode, 'plugin_statwheel');
    $this->Lexer->addPattern('<stat (?:.*?)>','plugin_statwheel');
//    $this->Lexer->addExitPattern('</statwheel>','plugin_statwheel');
  }

  function postConnect(){
    $this->Lexer->addExitPattern('</statwheel>','plugin_statwheel');
  }

  /**
   * Handle the match
   */
  function handle($match, $state, $pos, Doku_Handler &$handler) {
    global $ID;
    switch ($state) {
      case DOKU_LEXER_ENTER :
        $options['id'] = $ID;
        if(preg_match('/id=([A-Za-z0-9]+)/i', $match, $match_id))
        {
          $options['id'] .= ":".$match_id[1];
        }

        if(preg_match('/radius=([0-9]+)/i', $match, $match_))
        {
          $options['radius'] = intval($match_[1]);
        }
        else
        {
          $options['radius'] = $this->getConf('radius');
        }

        //if(preg_match('/legend=([A-Za-z0-9]+)/i', $match, $match_))
        //{
        //  $options['legend'] = filter_var($match_[1], FILTER_VALIDATE_BOOLEAN);
        //}
        //else
        //{
        //  $options['legend'] = $this->getConf('radius');
        //}

        if(preg_match('/stepsize=([0-9]+)/i', $match, $match_))
        {
          $options['stepsize'] = intval($match_[1]);
        }
        else
        {
          $options['radius'] = $this->getConf('radius');
        }

        if(preg_match('/valuestrokealpha=([0-9\.]+)/i', $match, $match_))
        {
          $options['valuestrokealpha'] = floatval($match_[1]);
        }
        else
        {
          $options['valuestrokealpha'] = $this->getConf('valuestrokealpha');
        }

        if(preg_match('/valuestrokecolor=([^\s>]+)/i', $match, $match_))
        {
            $options['valuestrokecolor'] = $this->_color2hex($this->_isValid($match_[1]));
        }
        else
        {
          $options['valuestrokealpha'] = $this->getConf('valuestrokealpha');
        }

        if(preg_match('/valuestrokesize=([0-9]+)/i', $match, $match_))
        {
            $options['valuestrokesize'] = intval($match_[1]);
        }
        else
        {
          $options['valuestrokealpha'] = $this->getConf('valuestrokealpha');
        }

        if(preg_match('/gradient-inner=([^\s>]+)/i', $match, $match_))
        {
            $options['gradient_inner'] = $this->_color2hex($this->_isValid($match_[1]));
        }
        else
        {
          $options['valuestrokealpha'] = $this->getConf('valuestrokealpha');
        }

        if(preg_match('/gradient-outer=([^\s>]+)/i', $match, $match_))
        {
          $options['gradient_outer'] = $this->_color2hex($this->_isValid($match_[1]));
        }
        else
        {
          $options['valuestrokealpha'] = $this->getConf('valuestrokealpha');
        }

        if(preg_match('/gridcolor=([^\s>]+)/i', $match, $match_))
        {
          $options['gridcolor'] = $this->_color2hex($this->_isValid($match_[1]));
        }
        else
        {
          $options['gridcolor'] = $this->getConf('gridcolor');
        }

        if(preg_match('/gridsize=([0-9]+)/i', $match, $match_))
        {
          $options['gridsize'] = intval($match_[1]);
        }
        else
        {
          $options['gridsize'] = $this->getConf('gridsize');
        }

        if(preg_match('/labels=([A-Za-z0-9]+)/i', $match, $match_))
        {
          $options['labels'] = filter_var($match_[1], FILTER_VALIDATE_BOOLEAN);
        }
        else
        {
          $options['labels'] = $this->getConf('labels');
        }

        if(preg_match('/labelcolor=([^\s>]+)/i', $match, $match_))
        {
          $options['labelcolor'] = $this->_color2hex($this->_isValid($match_[1]));
        }
        else
        {
          $options['labelcolor'] = $this->getConf('labelcolor');
        }

        //'labelfont' => 'Handel Gothic.ttf'
        if(preg_match('/labelfont=[\'"](.+)[\'"]/i', $match, $match_))
        {
          $options['labelfont'] = $match_[1];
        }
        else
        {
          $options['labelfont'] = $this->getConf('labelfont');
        }

        if(preg_match('/table=([A-Za-z0-9]+)/i', $match, $match_))
        {
          $options['table'] = $match_[1];
        }
        else
        {
          $options['table'] = $this->getConf('table');
        }

        $handler->statoptions = $options;
        return array($state,$options);

      case DOKU_LEXER_MATCHED :
        preg_match('/name=([A-Za-z0-9]+)/i', $match, $match_name);
        $options['name'] = $match_name[1];

        preg_match('/value=([0-9]+)/i', $match, $match_value);
        $options['value'] = $match_value[1];

        preg_match('/max=([0-9]+)/i', $match, $match_max);
        $options['max'] = $match_max[1];

        $handler->stats[] = $options;
        $options = array_merge($handler->statoptions, $options);
        return array($state, $options);

      case DOKU_LEXER_EXIT :
        $name = preg_replace("/:/", "/", $handler->statoptions['id']);
        $path = dirname ( $name );
        if (!file_exists(DOKU_INC . '/data/media/statwheel/' . $path)) {
          mkdir(DOKU_INC . '/data/media/statwheel/'.$path, 0777, true);
        }
        $options = array_merge($handler->statoptions, array('output'=> DOKU_INC . 'data/media/statwheel/' . $name . '.png') );
        generateStatWheel($handler->stats, $options);

        unset($handler->statoptions);
        unset($handler->stats);
        return array($state, $options);

      case DOKU_LEXER_UNMATCHED :break;
    }
  }

  /**
   * Create output
   */
  function render($mode, Doku_Renderer &$renderer, $data) {
    if($mode != 'xhtml') return false;
    list($state, $match) = $data;
    switch ($state) {
      case DOKU_LEXER_ENTER :
        if($match['table'])
        {
          $renderer->doc .= "<table>";
          $renderer->doc .= "<tr>";
          $renderer->doc .= "<td colspan='2'>";
        }
        // Create the parser
        $Parser = & new Doku_Parser();
        // Add the Handler
        $Parser->Handler = & new Doku_Handler();
        $Parser->addMode('media',new Doku_Parser_Mode_Media());
        $instructions = $Parser->parse("{{statwheel:".$match['id'].".png}}");
        foreach ( $instructions as $instruction ) {
          // Execute the callback against the Renderer
          call_user_func_array(array(&$renderer, $instruction[0]),$instruction[1]);
        }

        if($match['table'])
        {
          $renderer->doc .= "</td>"."</tr>";
        }
        return true;

      case DOKU_LEXER_MATCHED :
        if($match['table'])
        {
          $renderer->doc .= "<tr>";
          $renderer->doc .= "<td>".$match['name']."</td>";
          $renderer->doc .= "<td>".$match['value']."</td>";
          $renderer->doc .= "</tr>";
        }
        return true;

      case DOKU_LEXER_UNMATCHED :
        return true;

      case DOKU_LEXER_EXIT :
        if($match['table'])
        {
         $renderer->doc .= "</table>";
        }
        return true;
    }
    return false;
  }

  // validate color value $c
  // this is cut price validation - only to ensure the basic format is correct and there is nothing harmful
  // three basic formats  "colorname", "#fff[fff]", "rgb(255[%],255[%],255[%])"
  function _isValid($c) {
    $c = trim($c);

    $pattern = "/^\s*(
          ([a-zA-Z]+)|                                #colorname - not verified
          (\#([0-9a-fA-F]{3}|[0-9a-fA-F]{6}))|        #colorvalue
          (rgb\(([0-9]{1,3}%?,){2}[0-9]{1,3}%?\))     #rgb triplet
          )\s*$/x";

    if (preg_match($pattern, $c)) return $c;

    return "";
  }
  /**
   * Translate color names and RGB to hex values
   */
  function _color2hex($name)
  {
    static $colornames = null;
    if(is_null($colornames))
    {
      include dirname(__FILE__).'/colornames.php';
    }
    if(!preg_match('/^(#|rgb)/', $name) && array_key_exists($name, $colornames))
      return $colornames[$name];
    elseif(preg_match('/rgb\(([0-9]{1,3}%?),([0-9]{1,3}%?),([0-9]{1,3}%?)\)/', $name, $matches))
    {
      $colors = array();
      for($i=1;$i<4;$i++)
      {
        $percent = substr($matches[$i], -1, 1) == '%';
        $colors[$i] = $percent?(substr($matches[$i],0,-1)/100)*256:$matches[$i];
        if($colors[$i] === 256) $colors[$i] = 255;
      }
      return sprintf('#%02X%02X%02X', $colors[1], $colors[2], $colors[3]);
    }
    else
      return $name;
  }
}

//Setup VIM: ex: et ts=4 :
