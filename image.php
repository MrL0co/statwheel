<?php
//debug print
function drawImage(Imagick $im, $output) {
    $im->setCompressionQuality(100);
    $im->setImageFormat("png");

    if($output == 'echo')
    {
      header("Content-Type: image/" . $im->getImageFormat());
      echo $im;
      exit;
    }else
    {
      $im->writeImage($output);
    }
}

function generateStatWheel(array $stats, array $useroptions = array()) {
  /* Set default values for option and merge with user specified option */
  $options = array(
      'radius'   => 155
     ,'legend'   => false
     ,'stepsize' => 26
     ,'valuestrokealpha' => .5
     ,'valuestrokecolor' => '#045'
     ,'valuestrokesize'  => 2

     ,'gradient_inner' => "#ffd505"
     ,'gradient_outer' => "#f12d05"

     ,'gridcolor' => '#000'
     ,'gridsize'  => 2

     ,'labels'    => true
     ,'labelcolor' => '#000'
     //,'labelfont' => 'Handel Gothic.ttf'

     ,'output' => 'echo'
  );

  $options = array_merge($options, $useroptions);

  $wheelrad = $options['radius'] - 25;
  $widthtot = $options['radius'] * 2 + ($options['legend']?200:0);
  $height = $options['radius'] * 2;
  $width = $options['radius'] * 2;
  $x = $options['radius'];
  $y = $options['radius'];
  $steps = array();
  for($i = $wheelrad; $i > 0; $i -= $options['stepsize'] ) { $steps[] = $i; }
  $sides = count($stats);
  $stats[] = $stats[0];//filling fix
  $step = 360/$sides;
  $co_ords = array();

  /* Create an Imagick object with transparent canvas */
  $circle = new Imagick();
  $circle->newImage($width, $height, new ImagickPixel('transparent'));
  $circle->setImageFormat('png');
  /* New ImagickDraw instance for circle draw */
  $circledraw = new ImagickDraw();
  /* Set fill color for circle */
  $circledraw->setFillColor('#FFFFFF');
  /* Set circle dimensions */
  $circledraw->circle($x, $y, $x, $y + $options['radius'] - 1);
  /* Draw circle onto the canvas */
  $circle->drawImage($circledraw);

  /* Create gradient backdrop*/
  $gradient = new Imagick();
  $gradient->newPseudoImage($width, $height, "radial-gradient:".$options['gradient_inner']."-".$options['gradient_outer']);
  $gradient->setImageFormat('png');
  /* Mask gradient to circle */
  $gradient->compositeImage($circle, Imagick::COMPOSITE_COPYOPACITY, 0, 0);

  /* Create Grid */
  $grid = new ImagickDraw();
  $grid->setFillColor(new ImagickPixel('transparent'));
  $grid->setStrokeColor($options['gridcolor']);
  $grid->setStrokeWidth( $options['gridsize'] );

  /* Generate 'Round' grid layers */
  foreach($steps as $index => $radius2)
  {
    for($i = -90; $i <= (270+1); $i += $step)
    {
      $rad = ($i * M_PI / 180);
      $co_ords[$index][] = array( 'x' => $x + ( cos($rad) * ($radius2 - $options['gridsize']) )
                                 ,'y' => $y + ( sin($rad) * ($radius2 - $options['gridsize']) ) );
    }
    $grid->polyline($co_ords[$index]);
  }

  /* Generate 'spokes' grid */
  foreach ($co_ords[0] as $coord) {
    $grid->line($x, $y, $coord['x'], $coord['y']);
  }

  /* Generate 'value' overlay */
  for($i = -90, $index = 0; $i <= (270+1); $i += $step, $index++)
  {
    $radius4 = ($stats[$index]['value'] / $stats[$index]['max']) * $wheelrad;
    $rad = ($i * M_PI / 180);
    $statsco_ords[] = array( 'x' => $x + ( cos($rad) * ($radius4 - $options['valuestrokesize']) )
                            ,'y' => $y + ( sin($rad) * ($radius4 - $options['valuestrokesize']) ) );
  }

  $grid->setFillColor($options['valuestrokecolor']);
  $grid->setFillAlpha($options['valuestrokealpha']);
  $grid->setStrokeWidth($options['valuestrokesize']);
  $grid->setStrokeColor($options['valuestrokecolor']);
  $grid->polyline($statsco_ords);

  /* Create labels */
  if($options['labels'])
  {
    $labels = new ImagickDraw();
    if($options['labelfont'])
    {
      if(!$labels->setFont($options['labelfont']))
      {
        return;
      }
    }
    $labels->setFillColor($options['labelcolor']);

    /* Set font size to 52 */
    $labels->setFontSize(12);
    $labels->setGravity(Imagick::GRAVITY_CENTER);

    for($i = -90, $index = 0; $i < 270; $i += $step, $index++)
    {
      $radius4 = 10 + $wheelrad;
      if($i <= 0 || $i>=180)
      {
        $labels->annotation( 0, -$radius4, $stats[$index]['name'] );
        $labels->rotate ( $step );
      }else
      {
        $labels->rotate ( -180 );
        $labels->annotation( 0, $radius4, $stats[$index]['name'] );
        $labels->rotate ( 180 + $step );
      }
    }
  }

  /* Combine backdrop, labels end grid */
  $img = new Imagick();
  $img->newImage($widthtot, $height, new ImagickPixel('transparent'));
  $img->setImageFormat('png');
  $img->compositeImage($gradient, Imagick::COMPOSITE_BLEND,0 ,0 );
  $img->drawImage($grid);
  if($options['labels'])
  {
    $img->drawImage($labels);
  }
  /* Output result */
  drawImage($img, $options['output']);
}
